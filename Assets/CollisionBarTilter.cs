﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionBarTilter : MonoBehaviour
{
	public float flatPercentage;		// percentage considered flat for angled rebound effect (as if ends of paddle were angled / rounded)
	public float maxTiltAngle;			// max angle ball is tilted (if hits bar ends)

	/// <summary>
	/// 
	/// </summary>
	/// <param name="collision">Collision.</param>
	public float CollisionTiltAngle(Collision collision)
	{
		var collisionPoint = collision.contacts[0].point.x;
		//var percentFromCentre = PercentFromCentreX(collision.contacts[0].point);
		
		float barHalfWidth = transform.localScale.x * 0.5f;
		float barCentreX = transform.position.x;
		
		float collisionFromCentre = collisionPoint - barCentreX;		// negative = left of centre
		var percentFromCentre = collisionFromCentre / barHalfWidth;

		Debug.Log("PercentFromCentreY: " + percentFromCentre);

		if (Mathf.Abs(percentFromCentre) > flatPercentage)
		{
			var tiltLength = barHalfWidth - flatPercentage;        // length of tilt section of bar
			var tiltDistance = percentFromCentre - flatPercentage;	// collision distance from start of tilt section
			
			return maxTiltAngle * (tiltDistance / tiltLength);
		}
		else
			return 0;
	}
	
	//private float PercentFromCentreX(Vector3 collisionPoint)
	//{
	//	float barHalfWidth = transform.localScale.x * 0.5f;
	//	float barCentreX = transform.position.x;
		
	//	float collisionFromCentre = collisionPoint.x - barCentreX;		// negative = left of centre
	//	float collisionWidthPercentage = collisionFromCentre / barHalfWidth;

	//	Debug.Log("PercentFromCentreY: " + collisionWidthPercentage);
	//	return collisionWidthPercentage;
	//}
}
