﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The 'ball' in this adaptation of Breakout
/// </summary>
[RequireComponent(typeof(PerfectRebound))]

public class PlasmaCharge : MonoBehaviour
{
	public float initialSpeed;
	public float launchAngleMax;  						// +/- from vertical
	public float launchAngleMin;  				
	
	public AudioClip brickAudio;
	public AudioClip energyBarAudio;
	public AudioClip wallAudio;
	public AudioClip riverAudio;
	public AudioClip riverLaunchAudio;
	
	public float moveRowTime;							// time to tween between rows
	public float riverHoldSeconds = 3f;
		
	private bool tweeningBallRow;

	private Vector3 startPosition;
	private bool inPlay = false;
	
	private Rigidbody rb;


	private void Start()
	{
		rb = GetComponent<Rigidbody>();
		startPosition = transform.position;
	}
	
	private void OnEnable()
	{
		BreakoutEvents.OnCityBuilt += OnCityBuilt;
		BreakoutEvents.OnEnergyBarSetTowerRow += OnEnergyBarSetTowerRow;
		BreakoutEvents.OnRestartGame += Reset;
		
		BreakoutEvents.OnChargesExpired += OnChargesExpired;
		BreakoutEvents.OnCityCleared += OnCityCleared;
	}

	private void OnDisable()
	{
		BreakoutEvents.OnCityBuilt -= OnCityBuilt;
		BreakoutEvents.OnEnergyBarSetTowerRow -= OnEnergyBarSetTowerRow;
		BreakoutEvents.OnRestartGame -= Reset;
				
		BreakoutEvents.OnChargesExpired -= OnChargesExpired;
		BreakoutEvents.OnCityCleared -= OnCityCleared;
	}
	

	public void SetRandomLaunchAngle()
	{
		float launchAngleZ = UnityEngine.Random.Range(-launchAngleMax, launchAngleMax);

		if (Mathf.Abs(launchAngleZ) < launchAngleMin)
			launchAngleZ = launchAngleZ <= 0 ? -launchAngleZ : launchAngleZ;
			
		transform.eulerAngles = new Vector3(0, 0, launchAngleZ);

		inPlay = true;
	}
	
	private void OnEnergyBarSetTowerRow(EnergyBar energyBar, TowerRow rowBelow, int rowIndex)
	{
		tweeningBallRow = true;
	
		LeanTween.moveZ(gameObject, rowBelow.transform.position.z, moveRowTime)
			.setEase(LeanTweenType.easeOutBounce)
			.setOnComplete(() =>
			{
				tweeningBallRow = false;
				GetComponent<Renderer>().material = rowBelow.EnergyBarMaterial;

				SetTowerRowSpeed(rowBelow, false);
				tweeningBallRow = false;
				BreakoutEvents.OnConsolidateCity?.Invoke();
			});
	}


	private void OnCollisionEnter(Collision collision)
	{
		if (inPlay)
		{
			if (collision.gameObject.CompareTag("TowerBrick"))
			{
				AudioSource.PlayClipAtPoint(brickAudio, Vector3.zero);

				var brickHit = collision.gameObject.GetComponent<TowerBrick>();
				brickHit.DemolishTowerLevel();
			}
			else if (collision.gameObject.CompareTag("Paddle"))
			{
				AudioSource.PlayClipAtPoint(energyBarAudio, Vector3.zero);
			}
			else if (collision.gameObject.CompareTag("Wall"))
			{
				AudioSource.PlayClipAtPoint(wallAudio, Vector3.zero);
			}
			else if (collision.gameObject.CompareTag("River"))
			{
				AudioSource.PlayClipAtPoint(riverAudio, Vector3.zero);
				BreakoutEvents.OnChargeLost?.Invoke(this);
			}
		}
	}

	private void SetTowerRowSpeed(TowerRow rowBelow, bool launch)
	{
		if (!inPlay)
			return;
			
		// calculate ball speed according to TowerRow
		var towerRowSpeed = initialSpeed * rowBelow.PlasmaChargeSpeedFactor;

		if (launch)
			rb.velocity = transform.up * towerRowSpeed;
		else
			rb.velocity = rb.velocity.normalized * towerRowSpeed;  		// change speed, keeping current direction

		//Debug.Log("plasmaChargeSpeedFactor = " + rowBelow.plasmaChargeSpeedFactor + ", rowScore = " +rowBelow.rowScore);
	}

	public void Reset()
	{
		Stop();
		transform.position = startPosition;
	}
    
    
  //  private IEnumerator HoldThenLaunchUp()
  //  {
		//yield return new WaitForSeconds(riverHoldSeconds);
	
	 //   var lastFrameSpeed = lastFrameVelocity.magnitude;
		//rb.velocity = Vector3.up * lastFrameSpeed;  // reflect at same speed exactly
		
		//AudioSource.PlayClipAtPoint(riverLaunchAudio, Vector3.zero);
		//yield return null;
    //}


	private void OnCityBuilt(CityBuilder city)
	{
		SetRandomLaunchAngle();
		SetTowerRowSpeed(city.towerRows[0], true);
	}
	
	private void OnChargesExpired()
	{
		Stop();
	}
	
	private void OnCityCleared()
	{
		Stop();
	}

	public void Stop()
	{
		rb.velocity = Vector3.zero;
		inPlay = false;
	}
}
