﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBrick : MonoBehaviour
{
	public TowerLevel parentLevel;
	public ParticleSystem demolishParticles;

	public void DemolishTowerLevel()
	{
		parentLevel.Demolish();		
	}

	public void PlayDemolishParticles()
	{
		demolishParticles.Play();
	}
}
