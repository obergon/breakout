﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarCollider : MonoBehaviour
{
	private void OnCollisionEnter(Collision collision)
	{
		// fire collision event

		if (collision.gameObject.CompareTag("Wall"))
		{
			BreakoutEvents.OnBarWallCollision?.Invoke(collision);
		}
		else if (collision.gameObject.CompareTag("Ball"))
		{
			BreakoutEvents.OnBarChargeCollision?.Invoke(collision);
		}
	}
}
