﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraController : MonoBehaviour
{
	public bool followEnergyBar = true;			// step in/out with paddle
	public float towerRowInterval;				// distance to move between tower rows
	public float moveRowDelay;
	public float moveRowTime;

	private float towerRowIndex;
	
	private Vector3 startPosition;

	private void Start()
	{
		startPosition = transform.position;
	}
	
	private void OnEnable()
	{
		BreakoutEvents.OnEnergyBarSetTowerRow += OnEnergyBarSetTowerRow;
	}

	private void OnDisable()
	{
		BreakoutEvents.OnEnergyBarSetTowerRow -= OnEnergyBarSetTowerRow;
	}

	private void OnEnergyBarSetTowerRow(EnergyBar energyBar, TowerRow rowBelow, int rowIndex)
	{
		towerRowIndex = rowIndex;
		
		if (followEnergyBar)
		{
			LeanTween.moveZ(gameObject, startPosition.z + (towerRowIndex * towerRowInterval), moveRowTime)
					.setDelay(moveRowDelay)
					.setEase(LeanTweenType.easeInOutSine);
			//.setOnComplete(() =>
			//{
			//});
		}
	}
}
