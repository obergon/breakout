﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]

public class PerfectRebound : MonoBehaviour
{
	public float horizontalTolerance;			// considered horizontal if rebound angle very closet to inward angle
	public float horizontalTilt;				// degrees to rotate to break stuck in horizontal pattern
	
	private Vector3 lastFrameVelocity;          // to maintain constant speed
	private Rigidbody rb;


    static readonly Vector3[] ValidDirections = {
		Vector3.up,
        Vector3.down,
        Vector3.left,
        Vector3.right,
        (Vector3.up + Vector3.right).normalized,
        (Vector3.up + Vector3.left).normalized,
        (Vector3.down + Vector3.right).normalized,
        (Vector3.down + Vector3.left).normalized,
    };

    private void Start()
	{
		rb = GetComponent<Rigidbody>();
	}
	
	private void Update()
    {
        lastFrameVelocity = rb.velocity;
    }
    
    private void OnCollisionEnter(Collision collision)
	{
		Reflect(collision);
	}

	private void Reflect(Collision collision)
	{
		Vector3 collisionNormal = ClampTo45(collision.contacts[0].normal);
		//Vector3 collisionNormal = collision.contacts[0].normal;

		var lastFrameSpeed = lastFrameVelocity.magnitude;
		var lastFrameNormal = lastFrameVelocity.normalized;
		var newDirection = Vector3.Reflect(lastFrameNormal, collisionNormal);
		//Debug.DrawRay(collision.contacts[0].point, collisionNormal, Color.cyan, 1.5f);
		//Debug.Log(collision.contacts.Length);

		// check for near-zero y-axis rebound and rotate slightly
		var desiredVelocity = newDirection * lastFrameSpeed;

		if (Mathf.Abs(desiredVelocity.y) < horizontalTolerance)
		{
			// rotate downwards by a small amount
			desiredVelocity = Quaternion.Euler(0, 0, horizontalTilt) * desiredVelocity;
			Debug.Log("Reflect horizontal: tilted to " + horizontalTilt);
		}

        var collisionBar = collision.gameObject.GetComponent<CollisionBarTilter>();
		
		if (collisionBar != null)
		{
			var barTiltAngle = collisionBar.CollisionTiltAngle(collision);
			
			if (Mathf.Abs(barTiltAngle) > 0)
			{
				desiredVelocity = Quaternion.Euler(0, 0, -barTiltAngle) * desiredVelocity;
				Debug.Log("Reflect bar: tilted by " + -barTiltAngle);
			}
		}
		
        rb.velocity = desiredVelocity; 		// reflect at same speed exactly
    }


	/// <summary>
	/// Clamps the given vector to the nearest 45 deg angle by finding the
	/// closest to 
	/// </summary>
	/// <returns>The to45.</returns>
	/// <param name="v">V.</param>
    private Vector3 ClampTo45(Vector3 v)
    {
        Vector3 result = ValidDirections[0];
        var normal = v.normalized;
        float nearestNormal = Vector3.Dot(normal, ValidDirections[0]);		// .Dot returns 1 to -1 to indicate closeness to vertical (0 is horizontal)

        for (int i = 1; i < ValidDirections.Length; i++)
        {
            var dotToCompare = Vector3.Dot(normal, ValidDirections[i]);
            
            if (dotToCompare > nearestNormal)
            {
                nearestNormal = dotToCompare;
                result = ValidDirections[i];
            }
        }

        return result;
    }
}
