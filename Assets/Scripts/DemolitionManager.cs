﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemolitionManager : MonoBehaviour
{
	//public int startCharges;
	public AudioClip gameOverAudio;  	
	public AudioClip cityClearedAudio;

	public AudioSource emptyWind;
	public AudioSource birdsong;

	private int demolitionScore;
	private int timerSeconds;
	private int misses;
	//private int levelsLeft;
	
	private int currentScore;
	private int finalScore;
		
	private bool timerPaused = false;

	private IEnumerator timerCoroutine = null;


	private void OnEnable()
	{
		//BreakoutEvents.OnRestartGame += Reset;
		BreakoutEvents.OnBuildCity += OnBuildCity;
		BreakoutEvents.OnCityBuilt += OnCityBuilt;
		BreakoutEvents.OnUpdateScore += OnUpdateScore;
		BreakoutEvents.OnUpdateTimer += OnUpdateTimer;
		BreakoutEvents.OnChargeLost += OnChargeLost;
		
		//BreakoutEvents.OnTowerLevelBuilt += OnTowerLevelBuilt;
		//BreakoutEvents.OnTowerLevelDemolished += OnTowerLevelDemolished;
		
		BreakoutEvents.OnChargesExpired += OnChargesExpired;
		BreakoutEvents.OnCityCleared += OnCityCleared;
	}



	private void OnDisable()
	{
		//BreakoutEvents.OnRestartGame -= Reset;
		BreakoutEvents.OnBuildCity -= OnBuildCity;
		BreakoutEvents.OnCityBuilt -= OnCityBuilt;
		BreakoutEvents.OnUpdateScore -= OnUpdateScore;
		BreakoutEvents.OnUpdateTimer -= OnUpdateTimer;
		BreakoutEvents.OnChargeLost -= OnChargeLost;
		
		//BreakoutEvents.OnTowerLevelBuilt -= OnTowerLevelBuilt;
		//BreakoutEvents.OnTowerLevelDemolished -= OnTowerLevelDemolished;
		
		BreakoutEvents.OnChargesExpired -= OnChargesExpired;
		BreakoutEvents.OnCityCleared -= OnCityCleared;
	}

	private void OnTowerLevelBuilt()
	{
		//levelsLeft++;
		//BreakoutEvents.OnTowerLevelsLeftUpdated?.Invoke(levelsLeft);
	}
	
	private void OnTowerLevelDemolished(Tower parentTower)
	{
		//levelsLeft--;
		//BreakoutEvents.OnTowerLevelsLeftUpdated?.Invoke(levelsLeft);
		
		//if (levelsLeft <= 0)
			//BreakoutEvents.OnCityCleared?.Invoke();
	}

	private void StopTimer()
	{
		if (timerCoroutine != null)
		{
			StopCoroutine(timerCoroutine);
			timerCoroutine = null;
		}
		
		timerPaused = true;
	}
	
	private void ResetTimer()
	{
		StopTimer();
		timerPaused = false;
		
		timerSeconds = 0;
		BreakoutEvents.OnTimerUpdated?.Invoke(timerSeconds);
	}
	
	private void StartTimer()
	{
		StopTimer();
		ResetTimer();
				
		timerPaused = false;

		timerCoroutine = StartTimeCount();
		StartCoroutine(timerCoroutine);
	}

	private IEnumerator StartTimeCount()
	{
		while (!timerPaused)
		{
			yield return new WaitForSeconds(1);
			timerSeconds++;
			BreakoutEvents.OnTimerUpdated?.Invoke(timerSeconds);
		}

		yield return null;
	}

	private void OnBuildCity(CityBuilder city)
	{
		ResetScore();
	}
	
	private void OnCityBuilt(CityBuilder city)
	{
		StartTimer();
	}

	private void OnChargeLost(PlasmaCharge charge)
	{
		AddMiss();
		
		if (misses <= 0)
			BreakoutEvents.OnChargesExpired?.Invoke(); 			// game over
	}
	
	private void AddMiss()
	{
		misses++;
		BreakoutEvents.OnMissesUpdated?.Invoke(misses);
		
		CalculateCurrentScore(false);
	}
	
	private void OnChargesExpired()
	{
		StopTimer();
		AudioSource.PlayClipAtPoint(gameOverAudio, Vector3.zero);
	}
	
	private void OnCityCleared()
	{
		StopTimer();
		AudioSource.PlayClipAtPoint(cityClearedAudio, Vector3.zero);

		CalculateCurrentScore(true);
	}

	private void CalculateCurrentScore(bool final)
	{
		//finalScore = demolitionScore / (timerSeconds > 0 ? timerSeconds : 1) / (misses > 0 ? misses : 1);	
		
		int newScore = (demolitionScore - timerSeconds) / (misses > 0 ? misses : 1);

		currentScore = newScore;
		BreakoutEvents.OnCurrentScore?.Invoke(currentScore);
			
		if (final)
		{
			finalScore = newScore;
			BreakoutEvents.OnFinalScore?.Invoke(finalScore);
		}
	}

	private void OnUpdateTimer()
	{
		timerSeconds++;
		BreakoutEvents.OnTimerUpdated?.Invoke(timerSeconds);
		
		CalculateCurrentScore(false);
	}

	private void OnUpdateScore(int increment)
	{
		demolitionScore += increment;
		BreakoutEvents.OnScoreUpdated?.Invoke(demolitionScore);
		
		CalculateCurrentScore(false);
	}

	
	private void ResetScore()
	{
		demolitionScore = 0;
		timerSeconds = 0;
		misses = 0;
		//levelsLeft = 0;
		currentScore = 0;
		finalScore = 0;

		BreakoutEvents.OnMissesUpdated?.Invoke(misses);
		BreakoutEvents.OnTimerUpdated?.Invoke(timerSeconds);
		BreakoutEvents.OnScoreUpdated?.Invoke(demolitionScore);
		BreakoutEvents.OnCurrentScore?.Invoke(currentScore);
		//BreakoutEvents.OnTowerLevelsLeftUpdated?.Invoke(levelsLeft);

		ResetTimer();
	}
}
