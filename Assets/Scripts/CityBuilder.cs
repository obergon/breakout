﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityBuilder : MonoBehaviour
{
	public Transform frontLeft;       // far left of front row of towers
	public Transform City;              // empty G/O

	public Transform TopWall;
	public Transform BottomWall;
	public Transform LeftWall;
	public Transform RightWall;
	
	public AudioClip brickAudio;

	public int maxRowWidth;      		// in bricks, to contrain city width

	public int rowsPerCity;             // number of rows in the city
	public int totalTowerLevels;      // fixed number of TowerLevels in a city. levels equally distributed across rows.
		
	public float rowSpacing;
	public float towerSpacing;
	public float brickBuildDelay;

	public int fixedTowerWidth; 	   // tower bricks
	public int maxTowerWidth;          // tower bricks
	public int minTowerLevels;     		// in a tower
	public int maxTowerLevels;         // in a tower

	public int rowScoreBase;
	public int rowScoreInterval;
	public float rowSpeedIncrement;				// factor by which each row increases ball speed
	public float rowEnergyBarSpeedIncrement;	// factor by which each row increases paddle move speed

	public float cityToPaddleY = 2.5f;			// distance between city ground level and paddle underneath
	public float towerRowOffset = 0.25f;		// stagger tower position

	public int levelsPerRow { get { return totalTowerLevels / rowsPerCity; } }       // number of tower levels in each row

	public GameObject towerRowPrefab;
	public GameObject towerPrefab;
	public GameObject towerLevelPrefab;
	public List<GameObject> towerBrickPrefabs;      	// different styles
	public List<Material> energyBarRowMaterials;     	// paddle changes colour to match row it's under

	public float paddleLimitMargin;					// distance from left/right wall to paddle movement limit

	private float farRightTowerX = 0;               // ie. furthest edge of city (right)
	private float farLeftTowerX = 0;                // ie. furthest edge of city (left)

	private float wallMargin = 0.25f;				// half a brick .. would be better to use actual brick width 

	public List<TowerRow> towerRows { get; private set; }


	private void Start()
	{
		RestartGame();
	}
	
	private void OnEnable()
	{
		BreakoutEvents.OnConsolidateCity += OnConsolidateCity;
		BreakoutEvents.OnRestartGame += RestartGame;
		
		BreakoutEvents.OnTowerLevelDemolished += OnTowerLevelDemolished;
	}

	private void OnDisable()
	{
		BreakoutEvents.OnConsolidateCity -= OnConsolidateCity;
		BreakoutEvents.OnRestartGame -= RestartGame;
		
		BreakoutEvents.OnTowerLevelDemolished -= OnTowerLevelDemolished;
	}


	// consolidate all towers once energyBar & plasmaCharge have moved between rows
	private void OnConsolidateCity()
	{
		foreach (var row in towerRows)
		{
			row.ConsolidateRow();
		}
	}
	
	private void OnTowerLevelDemolished(Tower parentTower)
	{
		var levelsLeft = TowerLevelsLeft;
		
		BreakoutEvents.OnTowerLevelsLeftUpdated?.Invoke(levelsLeft);		// TODO?
	
		if (levelsLeft <= 0)
			BreakoutEvents.OnCityCleared?.Invoke();
	}
	
	
	private void RestartGame()
	{
		foreach (Transform towerRow in City)
		{
     		Destroy(towerRow.gameObject);
 		}
		
		towerRows = new List<TowerRow>();	
		BreakoutEvents.OnTowerLevelsLeftUpdated?.Invoke(TowerLevelsLeft);		// TODO?
		
		StartCoroutine(BuildCity());
	}


	private IEnumerator BuildCity()
	{
		BreakoutEvents.OnBuildCity?.Invoke(this);
	
		for (int i = 0; i < rowsPerCity; i++)
		{
			// stagger every second row
			var xPosition = frontLeft.position.x + ((i % 2 > 0) ? 0f : towerRowOffset);
			var rowPosition = new Vector3(xPosition, frontLeft.position.y, frontLeft.position.z + (rowSpacing * (i + 1)));

			yield return StartCoroutine(BuildTowerRow(rowPosition, i, false));
		}

		PositionSideWalls();

		//BreakoutEvents.OnTowerLevelsLeftUpdated?.Invoke(TowerLevelsLeft);		// TODO?
		BreakoutEvents.OnCityBuilt?.Invoke(this);
	}


	private IEnumerator BuildTowerRow(Vector3 rowPosition, int rowIndex, bool randomWidth)
	{
		if (rowIndex >= towerBrickPrefabs.Count)
			yield break;

		var brickPrefab = towerBrickPrefabs[rowIndex];
		var brickWidth = brickPrefab.transform.localScale.x;

		Vector3 previousTowerPos = rowPosition;
		float previousTowerWidth = 0;

		int levelsBuilt = 0;
		var newTowerRow = Instantiate(towerRowPrefab, rowPosition, Quaternion.identity, City) as GameObject;
		var towerRow = newTowerRow.GetComponent<TowerRow>();

		// set row score and ball speed
		var rowNumber = rowIndex + 1;
		towerRow.SetRowData(rowNumber, rowScoreBase + (rowScoreInterval * rowIndex),
							1 + (rowIndex * rowSpeedIncrement), 1 + (rowIndex * rowEnergyBarSpeedIncrement), energyBarRowMaterials[rowIndex]);
		
		towerRows.Add(towerRow);
		
		int rowWidth = 0;
		int towerCount = 0;

		while (levelsBuilt < levelsPerRow)
		{
			var levelsLeft = levelsPerRow - levelsBuilt;
			towerCount++;

			var towerWidth = (randomWidth) ? UnityEngine.Random.Range(1, maxTowerWidth) : fixedTowerWidth;
			var towerHeight = UnityEngine.Random.Range(minTowerLevels, Mathf.Min(levelsLeft, maxTowerLevels));

			// don't exceed levelsPerRow
			if (towerHeight + levelsBuilt > levelsPerRow)
				towerHeight = levelsPerRow - levelsBuilt;

			if (randomWidth)
			{
				// if the tower to be built would extend beyond the max width of row
				// then reduce the tower width accordingly and increase its height
				if ((rowWidth + towerWidth) >= maxRowWidth)
				{
					towerWidth = maxRowWidth - rowWidth;
					towerHeight = levelsLeft;
				}
			}
			else
			{
				var maxTowers = maxRowWidth / towerWidth;
			
				if (towerCount == maxTowers)		// this is the last tower in the row, use all remaining levels
						towerHeight = levelsLeft;
			}
				
			var towerPosition = new Vector3(previousTowerPos.x + (previousTowerWidth * brickWidth) + towerSpacing, previousTowerPos.y, previousTowerPos.z);
			yield return StartCoroutine(BuildTower(towerPosition, towerWidth, towerHeight, rowIndex, newTowerRow.GetComponent<TowerRow>()));

			levelsBuilt += towerHeight;

			rowWidth += towerWidth;
			
			previousTowerPos = towerPosition;
			previousTowerWidth = towerWidth;
		}
	}

	/// <summary>
	/// Builds a tower
	/// </summary>
	/// <param name="towerPosition">Start position for first brick</param>
	/// <param name="width">Width of tower, in bricks</param>
	/// <param name="levels">Height of tower, in bricks</param>
	/// <param name="rowIndex">Index into towerBrickPrefabs list</param>
	private IEnumerator BuildTower(Vector3 towerPosition, int width, int levels, int rowIndex, TowerRow parentRow)
	{
		if (rowIndex >= towerBrickPrefabs.Count)
			yield break;

		var brickPrefab = towerBrickPrefabs[rowIndex];
		var newTowerObject = Instantiate(towerPrefab, towerPosition, Quaternion.identity, parentRow.transform) as GameObject;
		var brickHeight = brickPrefab.transform.localScale.y;
		var brickWidth = brickPrefab.transform.localScale.x;

		var newTower = newTowerObject.GetComponent<Tower>();
		parentRow.AddTower(newTower);
		newTower.SetParentRow(parentRow);
		
		for (int i = 0; i < levels; i++)
		{
			var levelPosition = new Vector3(towerPosition.x, towerPosition.y + (brickHeight * (i + 1)), towerPosition.z);
			yield return StartCoroutine(BuildTowerLevel(newTower.GetComponent<Tower>(), levelPosition, width, rowIndex, i));
				
			//yield return new WaitForSeconds(brickBuildDelay);
		}

		// update tower position limits for city limit wall positioning
		var farRight = towerPosition.x + (width * brickWidth) + brickWidth;     // half brick margin
		var farLeft = towerPosition.x - brickWidth;                             // half brick margin

		if (farRight > farRightTowerX)
			farRightTowerX = farRight;

		if (farLeft < farLeftTowerX)
			farLeftTowerX = farLeft;
			
		// UI update level count
		BreakoutEvents.OnTowerLevelsLeftUpdated?.Invoke(TowerLevelsLeft);		// TODO?
		AudioSource.PlayClipAtPoint(brickAudio, Vector3.zero);

		yield return null;
	}


	private IEnumerator BuildTowerLevel(Tower parentTower, Vector3 startPosition, int width, int rowIndex, int levelIndex)
	{
		if (rowIndex >= towerBrickPrefabs.Count)
			yield break;

		var brickPrefab = towerBrickPrefabs[rowIndex];
		var brickWidth = brickPrefab.transform.localScale.x;

		var newLevelObject = Instantiate(towerLevelPrefab, startPosition, Quaternion.identity, parentTower.transform) as GameObject;
		BreakoutEvents.OnTowerLevelBuilt?.Invoke();
		
		var newLevel = newLevelObject.GetComponent<TowerLevel>();
		newLevel.SetParentTower(parentTower, levelIndex);
		parentTower.AddLevel(newLevel);
		
		// level brick(s)
		for (int i = 0; i < width; i++)
		{
			var brickPosition = new Vector3(startPosition.x + (brickWidth * (i + 1)), startPosition.y, startPosition.z);
			var newBrickObject = Instantiate(brickPrefab, brickPosition, Quaternion.identity, newLevelObject.transform) as GameObject;
			var newBrick = newBrickObject.GetComponent<TowerBrick>();
			newBrick.parentLevel = newLevelObject.GetComponent<TowerLevel>();

			newLevel.AddLevelBrick(newBrick);
			//yield return new WaitForSeconds(brickBuildDelay);
		}

		yield return null;
	}

	private void PositionSideWalls()
	{
		LeftWall.position = new Vector3(farLeftTowerX + wallMargin, LeftWall.position.y, LeftWall.position.z);
		RightWall.position = new Vector3(farRightTowerX + wallMargin, RightWall.position.y, RightWall.position.z);
		
		// set paddle left/right limits for each row
		foreach (var row in towerRows)
		{
			row.paddleLimitLeft.position = new Vector3(LeftWall.position.x + paddleLimitMargin, frontLeft.position.y - cityToPaddleY, row.transform.position.z);
			row.paddleLimitRight.position = new Vector3(RightWall.position.x - paddleLimitMargin, frontLeft.position.y - cityToPaddleY, row.transform.position.z);
		}
	}
	
	
	private int TowerLevelsLeft
	{
		get
		{
			int levelsLeft = 0;
			foreach (var row in towerRows)
			{
				levelsLeft += row.TowerLevelsLeft;
			}
			return levelsLeft;
		}
	}
}
